package ro.metropolice.cesium.scheduler.management;

import java.util.LinkedHashMap;
import java.util.List;

import ro.metropolice.cesium.scheduler.ScheduledJob;
import ro.metropolice.cesium.scheduler.ScheduledJobParameters;
import ro.metropolice.cesium.scheduler.Scheduler;
import ro.metropolice.cesium.scheduler.SchedulerListner;
import ro.metropolice.cesium.scheduler.jobs.Job;
import ro.metropolice.cesium.scheduler.jobs.JobFactory;

public class SchedulerManager implements SchedulerListner {
	
	private Scheduler scheduler;
	private JobFactory factory;
	
	private LinkedHashMap<String, ManagedSchedulerJob> idToManagedJob = new LinkedHashMap<>();
			
	public void scheduleJobs(List<JobInformation> jobs){
		jobs.forEach(jobInfo -> scheduleJob(jobInfo));
	}

	public ManagedSchedulerJob scheduleJob(JobInformation jobInfo) {
		
		//TODO: check if job exists and only do an update		
		Job job = createJob(jobInfo);		
		ScheduledJobParameters params = getJobParameters(jobInfo);
		ManagedSchedulerJob mJob = scheduleJob(job, params);
		mJob.setJobInfo(jobInfo);
		return mJob ;
	}

	private ManagedSchedulerJob scheduleJob(Job job, ScheduledJobParameters params) {		
		ScheduledJob scheduledJob = scheduler.scheduleJob(job, params);		
		ManagedSchedulerJob mJob = new ManagedSchedulerJob(this, scheduledJob);
		idToManagedJob.put(params.getJobId(), mJob);		
		return mJob ;
	}

	private ScheduledJobParameters getJobParameters(JobInformation jobInfo) {
		ScheduledJobParameters scheduledJobParameters = new ScheduledJobParameters();
		scheduledJobParameters.setJobId(jobInfo.getDefinition().getJobId());
		scheduledJobParameters.setSchedule(jobInfo.getDefinition().getSchedule());
		
		//TODO: copy all the parameters
		
		return scheduledJobParameters;
	}

	private Job createJob(JobInformation jobInfo) {
		return factory.createJob(jobInfo);
	}
	
}
