package ro.metropolice.cesium.scheduler.jobs;

import ro.metropolice.cesium.scheduler.management.JobDefinition;
import ro.metropolice.cesium.scheduler.management.JobState;

public interface JobType {
	
	public Job createJob(JobDefinition definition);
	public JobState createDefaultJobState(JobDefinition definition);
	
}
