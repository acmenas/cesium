package ro.metropolice.cesium.scheduler;

import java.util.Date;

import ro.metropolice.cesium.scheduler.jobs.Job;

public class ScheduledJob {
	
	public enum JobStatus {
		UNKNOWN,
		ENABLED,
		DISABLED,
		SCHEDULED,
		CANCELED,
		RUNNING,
		ERROR, 
	}
	
	private Scheduler scheduler;
	private Job job;
	private ScheduledJobParameters parameters;
	
	private JobStatus status = JobStatus.UNKNOWN;	
	
	private int failedRunsCount = 0;	//how many times the task failed so far. On succesful running this is reset
	
	private Date lastRun;	//when is the last time the task has run
	private Date nextRun;	//when should run the next time according to the rule
	
	private Date startedAt;	//if the task is running this is the time it has started
	
	private Throwable lastException;
	private String lastStatusText;
		
	public ScheduledJob(Scheduler scheduler, Job createJob, ScheduledJobParameters parameters) {
		this.scheduler = scheduler;
		this.job = createJob;
		this.parameters = parameters;
	}

	public void shutdown() {
		job.shutdown();
	}
	
	public void kill(int timeoutmillis){
		
	}
	
	public int getFailedRunsCount() {
		return failedRunsCount;
	}

	public void setFailedRunsCount(int failedRunsCount) {
		this.failedRunsCount = failedRunsCount;
	}

	public Date getLastRun() {
		return lastRun;
	}

	public void setLastRun(Date lastRun) {
		this.lastRun = lastRun;
	}

	public Date getNextRun() {
		return nextRun;
	}

	public void setNextRun(Date nextRun) {
		this.nextRun = nextRun;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job createJob) {
		this.job = createJob;
	}

	public ScheduledJobParameters getParameters() {
		return parameters;
	}

	public void setParameters(ScheduledJobParameters parameters) {
		this.parameters = parameters;
	}
	
	public JobStatus getStatus() {
		return status;
	}

	void setStatus(JobStatus status) {
		this.status = status;
	}

	public Throwable getLastException() {
		return lastException;
	}

	public void setLastException(Throwable lastException) {
		this.lastException = lastException;
	}

	public String getLastStatusText() {
		return lastStatusText!=null ? lastStatusText : (lastException!=null ? lastException.getMessage() : null);
	}

	public void setLastStatusText(String lastStatusText) {
		this.lastStatusText = lastStatusText;
	}

}
