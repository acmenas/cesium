package ro.metropolice.cesium.scheduler.management;

import ro.metropolice.cesium.scheduler.ScheduledJob;

public class ManagedSchedulerJob {
	
	private SchedulerManager schedulerManager;
	private ScheduledJob scheduledJob;
	private JobInformation jobInfo;
	
	public ManagedSchedulerJob(SchedulerManager schedulerManager, ScheduledJob scheduledJob) {
		this.scheduledJob = scheduledJob;
		this.schedulerManager = schedulerManager;
	}

	public SchedulerManager getSchedulerManager() {
		return schedulerManager;
	}

	public void setSchedulerManager(SchedulerManager schedulerManager) {
		this.schedulerManager = schedulerManager;
	}

	public ScheduledJob getScheduledJob() {
		return scheduledJob;
	}

	public void setScheduledJob(ScheduledJob scheduledJob) {
		this.scheduledJob = scheduledJob;
	}

	public JobInformation getJobInfo() {
		return jobInfo;
	}

	public void setJobInfo(JobInformation jobInfo) {
		this.jobInfo = jobInfo;
	}

}
