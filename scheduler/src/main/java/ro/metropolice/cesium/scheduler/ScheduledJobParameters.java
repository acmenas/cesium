package ro.metropolice.cesium.scheduler;

import java.util.LinkedHashMap;
import java.util.Map;

public class ScheduledJobParameters {
	
	private String jobId;	
	private String schedule; 
	private Map<String, Object> parameters;
		
	public ScheduledJobParameters(String jobId, String schedule, Map<String, Object> parameters) {
		super();
		this.jobId = jobId;
		this.schedule = schedule;
		this.parameters = parameters;
	}	
	public ScheduledJobParameters(){
		parameters = new LinkedHashMap<>();
	}		
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}	
	public Object getParameter(String key) {
		return parameters.get(key);
	}	
	public void setParameter(String key, Object value) {
		parameters.put(key, value);
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getJobPath() {
		//TODO: extract the path from the ID
		return null;
	}
	public String getJobName() {
		// TODO: extract just the name from the ID
		return getJobId();
	}	
		
}
