package ro.metropolice.cesium.scheduler;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.metropolice.cesium.scheduler.ScheduledJob.JobStatus;
import ro.metropolice.cesium.scheduler.jobs.Job;
import ro.metropolice.cesium.scheduler.jobs.JobStarter;
import ro.metropolice.cesium.scheduler.jobs.RunningContext;

public class Scheduler {

	private static Logger log = LoggerFactory.getLogger(Scheduler.class);
	
	private SchedulerListner listner = new DefaultSchedulerListner();

	private Map<String,ScheduledJob> jobs = new LinkedHashMap<>();

	private org.quartz.Scheduler quartz;

	public void start() {
		try {			
			quartz.start();
			log.info("Job scheduler has started.");					
		} catch (Exception e) {
			log.error("Error starting job scheduler", e);		
		}
	}
		
	public void stop() {	
		try {			
			
			//TODO: shutdown all jobs
			
			quartz.shutdown();
			log.info("Job scheduler has shutdown.");
		} catch (Exception e) {
			log.error("Error shutting down job scheduler", e);
		}
	}

	public ScheduledJob scheduleJob(Job job, String id, String schedule, Map<String, Object> parameters) {
		return scheduleJob(job, new ScheduledJobParameters(id, schedule, parameters));
	}

	public ScheduledJob scheduleJob(Job job, ScheduledJobParameters params) {
		
		ScheduledJob sjob = cancelJob(params.getJobId());
		
		if(sjob==null){
			sjob = new ScheduledJob(this, job, params);
			jobs.put(params.getJobId(), sjob);
		}else {
			sjob.setScheduler(this);
			sjob.setJob(job);
			sjob.setParameters(params);
		}	
		
		scheduleInternalJob(sjob);
		
		return sjob;
	}
	
	private void scheduleInternalJob(ScheduledJob job) {
		try {

			JobDataMap jobDataMap = new JobDataMap();
			
			jobDataMap.put(JobStarter.SCHEDULED_JOB_QUARTZ_JOB_DATA_MAP_KEY, job);
			
			JobDetail quartzJob = JobBuilder.newJob(JobStarter.class)
					.usingJobData(jobDataMap)
					.withIdentity(job.getParameters().getJobName(), job.getParameters().getJobPath())					
					.build();
						
			Trigger trigger = createTriggerByRule(job, job.getParameters().getSchedule());

			if(trigger!=null){
				quartz.deleteJob(quartzJob.getKey());
				quartz.unscheduleJob(trigger.getKey());
				Date nextRun = quartz.scheduleJob(quartzJob, trigger);
				job.setStatus(JobStatus.SCHEDULED);
				job.setNextRun(nextRun);				
				log.info("Job " + job + " will run at " + nextRun);
			} else {
				job.setStatus(JobStatus.ENABLED);
				job.setNextRun(null);				
				log.info("Job " + job + " will NOT run unless explicitly started");
			}

		} catch (Exception e) {
			job.setNextRun(null);
			job.setLastStatusText("Error scheduling job " + e.getMessage());
			job.setLastException(e);
			job.setStatus(JobStatus.ERROR);
			log.error("Error scheduling job " + job, e);
		}				
	}

	private Trigger createTriggerByRule(ScheduledJob job, String rule) throws ParseException {			

		if(rule==null || rule.trim().isEmpty()){
			return null;
		} else{

			//TODO: replace here with JSON
			String[] ruleParts = rule.split("\\(");
			String ruleName = ruleParts[0];
			String paramCsv = ruleParts[1].split("\\)")[0].trim();
			String[] params = paramCsv.split(",");

			//instead of if's we could use a factory here for all supported rules
			if(ruleName.equals("cron")){
				Trigger trigger = TriggerBuilder.newTrigger()
						.withIdentity(getTriggerKey(job))
						.withSchedule(CronScheduleBuilder.cronSchedule(paramCsv))						
						.build();

				return trigger;
				
			} else if(ruleName.equals("now")) {
				return new SimpleTriggerImpl("nowtrigger", "internal", new Date());							
			} else {				
				return null;
			}
		}
	}
	
	public ScheduledJob cancelJob(String jobId) {
		ScheduledJob job = jobs.get(jobId);		
		if(job!=null){
			cancelJob(job);						
		}
		return job;
	}

	public void cancelJob(ScheduledJob sjob) {				
		try {
			
			sjob.setStatus(JobStatus.CANCELED);			
			Job job =  sjob.getJob();
			
			if(job!=null && job.isRunning()){
				throw new SchedulerException("Cannot cancel job:" + sjob.getParameters().getJobId() + " Job is still running.");
			}
			
			jobs.remove(sjob.getParameters().getJobId());
			quartz.deleteJob(getJobKey(sjob));
			quartz.unscheduleJob(getTriggerKey(sjob));
		} catch (org.quartz.SchedulerException e) {
			throw new SchedulerException("Error canceling job:" + sjob.getParameters().getJobId(), e);
		}
	}
	
	public void execute(ScheduledJob scheduledJob) {
		scheduledJob.getJob().execute(new RunningContext(scheduledJob));
	}


	private JobKey getJobKey(ScheduledJob job) {
		return new JobKey(job.getParameters().getJobName(), job.getParameters().getJobPath());
	}
	
	private TriggerKey getTriggerKey(ScheduledJob job) {
		return new TriggerKey(job.getParameters().getJobName(), job.getParameters().getJobPath());
	}

}
