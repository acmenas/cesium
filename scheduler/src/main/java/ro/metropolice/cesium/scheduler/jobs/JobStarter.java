package ro.metropolice.cesium.scheduler.jobs;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ro.metropolice.cesium.scheduler.ScheduledJob;

public class JobStarter implements org.quartz.Job {

	public static final String SCHEDULED_JOB_QUARTZ_JOB_DATA_MAP_KEY = ScheduledJob.class.getName();
	
	public void execute(JobExecutionContext context) throws JobExecutionException {		
		ScheduledJob scheduledJob = (ScheduledJob) context.getJobDetail().getJobDataMap().get(SCHEDULED_JOB_QUARTZ_JOB_DATA_MAP_KEY);
		scheduledJob.getScheduler().execute(scheduledJob);
	}

}