package ro.metropolice.cesium.scheduler;

public class SchedulerException extends RuntimeException {

	public SchedulerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SchedulerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SchedulerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SchedulerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SchedulerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
