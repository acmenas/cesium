package ro.metropolice.cesium.scheduler.management;

import java.util.LinkedHashMap;
import java.util.Map;

public class JobDefinition {
	
	private String jobId;
	
	private String name;
	private String path;
	
	private String jobType;
	
	private String schedule;
	
	private Map<String, String> parameters;
	
	public JobDefinition(){
		parameters = new LinkedHashMap<>();
	}
	
	public String getJobType() {
		return jobType;
	}
	
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	
	public String getSchedule() {
		return schedule;
	}
	
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public String getParameter(String key) {
		return parameters.get(key);
	}
	public void setParameter(String key, String value) {
		parameters.put(key, value);
	}
		
}
