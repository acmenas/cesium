package ro.metropolice.cesium.scheduler.management;

public class JobInformation {
	
	private JobDefinition definition;
	private JobState state;
	
	public JobDefinition getDefinition() {
		return definition;
	}
	public void setDefinition(JobDefinition definition) {
		this.definition = definition;
	}
	public JobState getState() {
		return state;
	}
	public void setState(JobState state) {
		this.state = state;
	}
	
	
}
